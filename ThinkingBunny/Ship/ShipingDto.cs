using System;
using System.Collections.Generic;

namespace ThinkingBunny.Ship
{
    public class ShipingDto
    {
        public readonly int Id;

        public readonly int StockId;

        public readonly DateTime ShipedAt;

        public ShipingDto(int id, int stockId, DateTime shipedAt)
        {
            Id = id;
            StockId = stockId;
            ShipedAt = shipedAt;
        }

        public override bool Equals(object obj)
        {
            return obj is ShipingDto dto &&
                   Id == dto.Id &&
                   StockId == dto.StockId &&
                   ShipedAt == dto.ShipedAt;
        }

        public override int GetHashCode()
        {
            int hashCode = -938402202;
            hashCode = hashCode * -1521134295 + EqualityComparer<int>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<int>.Default.GetHashCode(StockId);
            hashCode = hashCode * -1521134295 + ShipedAt.GetHashCode();
            return hashCode;
        }
    }
}