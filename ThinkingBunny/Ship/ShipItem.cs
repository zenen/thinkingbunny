using System;
using System.Transactions;
using ThinkingBunny.Warehouse;

namespace ThinkingBunny.Ship
{
    public class ShipItem<TItem>
    {
        private readonly IWarehouseRepository<TItem> warehouseRepository;

        private readonly IShipingRepository shipingRepository;

        public ShipItem(
            IWarehouseRepository<TItem> warehouseRepository,
            IShipingRepository shipingRepository
        )
        {
            this.warehouseRepository = warehouseRepository;
            this.shipingRepository = shipingRepository;
        }

        public ShipingDto Do(int warehouseId, int shelfId)
        {
            var warehouseDto = this.warehouseRepository.Find(warehouseId);
            if (warehouseDto is null)
            {
                throw new IndexOutOfRangeException();
            }
            var warehouse = new Warehouse<TItem>(warehouseDto);
            var shipping = warehouse.Ship(shelfId);

            using (var transaction = new TransactionScope())
            {
                this.shipingRepository.Save(shipping.ToDto());
                this.warehouseRepository.Save(warehouse.ToDto());
                transaction.Complete();
            }

            return shipping.ToDto();
        }
    }
}
