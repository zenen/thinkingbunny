using System;
using ThinkingBunny.Warehouse;

namespace ThinkingBunny.Ship
{
    class Shiping : IEquatable<Shiping>
    {
        internal readonly ID Id;

        internal readonly ID StockId;

        internal readonly DateTime ShipedAt;

        internal Shiping(ShipingDto dto) 
        {
            this.Id = dto.Id;
            this.StockId = dto.StockId;
            this.ShipedAt = dto.ShipedAt;
        }

        internal static Shiping Transit<TItem>(Stock<TItem> stock)
        {
            var dto = new ShipingDto(ID.NewId(), stock.Id, DateTime.Now);
            return new Shiping(dto);
        }

        public bool Equals(Shiping other)
        {
            return this.Id.Equals(other.Id);
        }

        internal ShipingDto ToDto()
        {
            return new ShipingDto(this.Id, this.StockId, this.ShipedAt);
        }
    }
}
