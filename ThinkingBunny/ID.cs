using System;

namespace ThinkingBunny
{
    public class ID
    {
        public static bool AutoGenerate = true;

        public const int Default = 0;

        private int Value;

        public static bool IsDefault(ID id) => id == Default;

        public bool IsDefault() => IsDefault(this);

        private ID(int id)
        {
            this.Value = id;
        }

        public static ID NewId()
        {
            if (AutoGenerate)
            {
                return BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
            }
            else
            {
                return Default;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is ID iD &&
                   Value == iD.Value;
        }

        public override int GetHashCode()
        {
            return -1937169414 + Value.GetHashCode();
        }

        public static implicit operator int(ID id) => id.Value;

        public static implicit operator ID(int id) => new ID(id);

        public static bool operator ==(ID self, ID other) => self.Equals(other);

        public static bool operator !=(ID self, ID other) => !self.Equals(other);
    }
}
