using System;
using System.Collections.Generic;

namespace ThinkingBunny.Warehouse
{
    public class StockDto<TItem>
    {
        public readonly int Id;

        public readonly TItem Item;

        public readonly DateTime StockedAt;

        public StockDto(int id, TItem item, DateTime stockedAt)
        {
            Id = id;
            Item = item;
            StockedAt = stockedAt;
        }

        public override bool Equals(object obj)
        {
            return obj is StockDto<TItem> dto &&
                   Id == dto.Id &&
                   EqualityComparer<TItem>.Default.Equals(Item, dto.Item) &&
                   StockedAt == dto.StockedAt;
        }

        public override int GetHashCode()
        {
            int hashCode = 1703632255;
            hashCode = hashCode * -1521134295 + EqualityComparer<int>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<TItem>.Default.GetHashCode(Item);
            hashCode = hashCode * -1521134295 + StockedAt.GetHashCode();
            return hashCode;
        }
    }
}