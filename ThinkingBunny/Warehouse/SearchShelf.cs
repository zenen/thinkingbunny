using System;
using System.Collections.Generic;

namespace ThinkingBunny.Warehouse
{
    public class SearchShelf<TItem>
    {
        private readonly IWarehouseRepository<TItem> warehouseRepository;

        public SearchShelf(
            IWarehouseRepository<TItem> warehouseRepository
        )
        {
            this.warehouseRepository = warehouseRepository;
        }

        public IEnumerable<ShelfDto<TItem>> Do(int warehouseId, ISearchCursor<TItem> cursor)
        {
            var warehouse = this.warehouseRepository.Find(warehouseId);
            if (warehouse is null)
            {
                throw new IndexOutOfRangeException();
            }
            return new Warehouse<TItem>(warehouse).Search(cursor);
        }
    }
}
