using System.Collections.Generic;

namespace ThinkingBunny.Warehouse
{
    public class WarehouseDto<TItem>
    {
        public readonly int Id;

        public readonly ShelfDto<TItem>[] Shelves;

        public WarehouseDto(int id, ShelfDto<TItem>[] shelves)
        {
            Id = id;
            Shelves = shelves;
        }

        public override bool Equals(object obj)
        {
            return obj is WarehouseDto<TItem> dto &&
                   Id == dto.Id &&
                   EqualityComparer<ShelfDto<TItem>[]>.Default.Equals(Shelves, dto.Shelves);
        }

        public override int GetHashCode()
        {
            int hashCode = 631223619;
            hashCode = hashCode * -1521134295 + EqualityComparer<int>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<ShelfDto<TItem>[]>.Default.GetHashCode(Shelves);
            return hashCode;
        }
    }
}