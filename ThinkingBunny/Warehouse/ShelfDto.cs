using System.Collections.Generic;

namespace ThinkingBunny.Warehouse
{
    public class ShelfDto<TItem>
    {
        public readonly int Id;

        public readonly StockDto<TItem> Stock;

        public ShelfDto(int id, StockDto<TItem> stock)
        {
            Id = id;
            Stock = stock;
        }

        public override bool Equals(object obj)
        {
            return obj is ShelfDto<TItem> dto &&
                   Id == dto.Id &&
                   EqualityComparer<StockDto<TItem>>.Default.Equals(Stock, dto.Stock);
        }

        public override int GetHashCode()
        {
            int hashCode = -2139857176;
            hashCode = hashCode * -1521134295 + EqualityComparer<int>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + Stock.GetHashCode();
            return hashCode;
        }
    }
}