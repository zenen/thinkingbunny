using System;

namespace ThinkingBunny.Warehouse
{
    public class StoreItem<TItem>
    {
        private readonly IWarehouseRepository<TItem> WarehouseRepo;

        public StoreItem(IWarehouseRepository<TItem> warehouseRepo)
        {
            this.WarehouseRepo = warehouseRepo;
        }

        public StockDto<TItem> Do(int warehouseId, int shelfId, TItem item)
        {
            var warehouseDto = this.WarehouseRepo.Find(warehouseId);
            if (warehouseDto is null)
            {
                throw new IndexOutOfRangeException();
            }
            var warehouse = new Warehouse<TItem>(warehouseDto);

            var stockDto = warehouse.Store(shelfId, item);

            this.WarehouseRepo.Save(warehouse.ToDto());

            return stockDto;
        }
    }
}
