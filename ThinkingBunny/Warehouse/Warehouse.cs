using System;
using System.Collections.Generic;
using System.Linq;
using ThinkingBunny.Ship;

namespace ThinkingBunny.Warehouse
{
    public class Warehouse<TItem> : IEquatable<Warehouse<TItem>>
    {
        internal readonly ID Id;

        private readonly IList<Shelf<TItem>> Shelves;

        internal Warehouse(WarehouseDto<TItem> dto)
        {
            this.Id = dto.Id;
            this.Shelves = dto.Shelves.Select(s => new Shelf<TItem>(s)).ToList();
        }

        internal StockDto<TItem> Store(ID shelfId, TItem item)
        {
            var shelf = this.GetShelf(shelfId);
            if (shelf is null)
            {
                throw new IndexOutOfRangeException();
            }
            var stock = Stock<TItem>.Transit(item);
            shelf.Load(stock);
            return stock.ToDto();
        }

        internal Shiping Ship(ID shelfId)
        {
            var shelf = this.GetShelf(shelfId);
            if (shelf is null)
            {
                throw new InvalidOperationException();
            }
            var stock = shelf.UnLoad();
            return Shiping.Transit(stock);
        }

        internal IEnumerable<ShelfDto<TItem>> Search(ISearchCursor<TItem> searchCursor)
        {
            foreach (var shelf in this.Shelves)
            {
                shelf.Accept(searchCursor);
            }
            return searchCursor.Result;
        }

        Shelf<TItem> GetShelf(ID shelfId)
        {
            return this.Shelves.SingleOrDefault(s => s.Id.Equals(shelfId));
        }

        public bool Equals(Warehouse<TItem> other)
        {
            return this.Id.Equals(other.Id);
        }

        internal WarehouseDto<TItem> ToDto()
        {
            return new WarehouseDto<TItem>(
                this.Id,
                this.Shelves.Select(s => s.ToDto()).ToArray());
        }
    }
}
