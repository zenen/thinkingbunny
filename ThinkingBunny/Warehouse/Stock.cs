using System;

namespace ThinkingBunny.Warehouse
{
    internal class Stock<TItem> : IEquatable<Stock<TItem>>
    {
        internal readonly ID Id;

        internal readonly TItem Item;

        internal readonly DateTime StockedAt;

        internal Stock(StockDto<TItem> dto)
        {
            this.Id = dto.Id;
            this.Item = dto.Item;
            this.StockedAt = dto.StockedAt;
        }

        internal static Stock<TItem> Transit(TItem item)
        {
            return new Stock<TItem>(new StockDto<TItem>(
                ID.NewId(),
                item,
                DateTime.Now));
        }

        public bool Equals(Stock<TItem> other)
        {
            return this.Id.Equals(other.Id);
        }

        internal StockDto<TItem> ToDto()
        {
            return new StockDto<TItem>(this.Id, this.Item, this.StockedAt);
        }
    }
}
