using System;

namespace ThinkingBunny.Warehouse
{
    class Shelf<TItem> : IEquatable<Shelf<TItem>>
    {
        private Stock<TItem> Stock;

        internal readonly ID Id;

        internal Shelf(ShelfDto<TItem> dto)
        {
            this.Id = dto.Id;
            this.Stock = dto.Stock is null ? null : new Stock<TItem>(dto.Stock);
        }

        internal void Load(Stock<TItem> stock)
        {
            if (!this.IsEmpty())
            {
                throw new InvalidOperationException();
            }
            this.Stock = stock;
        }

        internal Stock<TItem> UnLoad()
        {
            if (this.IsEmpty())
            {
                throw new InvalidOperationException();
            }
            var s = this.Stock.ToDto();
            this.Stock = null;
            return new Stock<TItem>(s);
        }

        internal bool IsEmpty()
        {
            return this.Stock is null;
        }

        internal void Accept(ISearchCursor<TItem> spec)
        {
            spec.Visit(this.ToDto());
        }

        public bool Equals(Shelf<TItem> other)
        {
            return this.Id.Equals(other.Id);
        }

        internal ShelfDto<TItem> ToDto()
        {
            return new ShelfDto<TItem>(this.Id, this.Stock?.ToDto());
        }
    }
}
