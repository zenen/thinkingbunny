namespace ThinkingBunny.Warehouse
{
    public interface IWarehouseRepository<TItem>
    {
        WarehouseDto<TItem> Find(int Id);

        void Save(WarehouseDto<TItem> warehouse);
    }
}
