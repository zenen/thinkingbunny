using System.Collections.Generic;

namespace ThinkingBunny.Warehouse
{
    public interface ISearchCursor<TItem>
    {
        IEnumerable<ShelfDto<TItem>> Result { get; }

        void Visit(ShelfDto<TItem> shelf);
    }
}
