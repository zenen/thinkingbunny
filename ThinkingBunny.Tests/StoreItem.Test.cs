using Xunit;
using Moq;
using System;

namespace ThinkingBunny.Warehouse.Tests;

public class StoreItemTests
{
    [Fact]
    public void StoreItem()
    {
        const int SHELF_ID = 1;
        var SHELF = new ShelfDto<string>(SHELF_ID, null);
        var WAREHOUSE = new WarehouseDto<string>(ID.NewId(), new ShelfDto<string>[] { SHELF });
        var repo = new Mock<IWarehouseRepository<string>>();
        repo
            .Setup(r => r.Find(It.IsAny<int>()))
            .Returns(WAREHOUSE);
        var service = new StoreItem<string>(repo.Object);

        var res = service.Do(ID.NewId(), SHELF_ID, "item");

        Assert.Equal("item", res.Item);
        repo.Verify(r => r.Save(It.IsAny<WarehouseDto<string>>()), Times.Exactly(1));
    }

    [Fact]
    public void When_shelf_has_already_stocked_then_should_throw()
    {
        const int SHELF_ID = 1;
        var STOCK = new StockDto<string>(ID.NewId(), "item", DateTime.Now);
        var SHELF = new ShelfDto<string>(SHELF_ID, STOCK);
        var WAREHOUSE = new WarehouseDto<string>(ID.NewId(), new ShelfDto<string>[] { SHELF });
        var repo = new Mock<IWarehouseRepository<string>>();
        repo
            .Setup(r => r.Find(It.IsAny<int>()))
            .Returns(WAREHOUSE);
        var service = new StoreItem<string>(repo.Object);

        Assert.Throws<InvalidOperationException>(() => service.Do(ID.NewId(), SHELF_ID, "item"));

        repo.Verify(r => r.Save(It.IsAny<WarehouseDto<string>>()), Times.Never());
    }

    [Fact]
    public void When_shelf_not_exists_on_location_then_should_throw()
    {
        const int EXPECTED_SHELF_ID = 1;
        const int ACTUAL_SHELF_ID = 2;
        var STOCK = new StockDto<string>(ID.NewId(), "item", DateTime.Now);
        var SHELF = new ShelfDto<string>(ACTUAL_SHELF_ID, STOCK);
        var WAREHOUSE = new WarehouseDto<string>(ID.NewId(), new ShelfDto<string>[] { SHELF });
        var repo = new Mock<IWarehouseRepository<string>>();
        repo
            .Setup(r => r.Find(It.IsAny<int>()))
            .Returns(WAREHOUSE);
        var service = new StoreItem<string>(repo.Object);

        Assert.Throws<IndexOutOfRangeException>(() => service.Do(ID.NewId(), EXPECTED_SHELF_ID, "item"));

        repo.Verify(r => r.Save(It.IsAny<WarehouseDto<string>>()), Times.Never());
    }

    [Fact]
    public void When_warehouse_not_exists_then_should_throw()
    {
        var shelfId = ID.NewId();
        var shelf = new ShelfDto<string>(
            shelfId, 
            new StockDto<string>(
                ID.NewId(),
                "item", 
                DateTime.Now));
        var repo = new Mock<IWarehouseRepository<string>>();
        repo
        .Setup(r => r.Find(It.IsAny<int>()))
        .Returns<WarehouseDto<string>?>(null);
        var service = new StoreItem<string>(repo.Object);

        Assert.Throws<IndexOutOfRangeException>(() => service.Do(ID.NewId(), shelfId, "item"));
        repo.Verify(r => r.Save(It.IsAny<WarehouseDto<string>>()), Times.Never());
    }
}
