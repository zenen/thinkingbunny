using Xunit;
using Moq;
using System;
using ThinkingBunny.Warehouse;

namespace ThinkingBunny.Ship.Tests;

public class ShipItemTests
{
    [Fact]
    public void ShopItem()
    {
        ID.AutoGenerate = false;
        const int STOCK_ID = 1;
        const int SHELF_ID = 1;
        const int WAREHOUSE_ID = 1;
        var STOCK = new StockDto<string>(STOCK_ID, "item", DateTime.Now);
        var SHELF = new ShelfDto<string>(SHELF_ID, STOCK);
        var WAREHOUSE = new WarehouseDto<string>(WAREHOUSE_ID, new ShelfDto<string>[] { SHELF });
        var warehouseRepo = new Mock<IWarehouseRepository<string>>();
        warehouseRepo
            .Setup(r => r.Find(It.IsAny<int>()))
            .Returns(WAREHOUSE);
        var shipingRepo = new Mock<IShipingRepository>();
        shipingRepo
            .Setup(r => r.Save(It.Is<ShipingDto>(s => s.Id.Equals(0) && s.StockId.Equals(STOCK_ID))));
        var service = new ShipItem<string>(warehouseRepo.Object, shipingRepo.Object);

        var res = service.Do(WAREHOUSE_ID, SHELF_ID);

        warehouseRepo.Verify(r => r.Save(It.IsAny<WarehouseDto<string>>()), Times.Exactly(1));
        shipingRepo.Verify(r => r.Save(It.IsAny<ShipingDto>()), Times.Exactly(1));
        shipingRepo.VerifyAll();
    }
}
